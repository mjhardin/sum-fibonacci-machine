//
//  AppDelegate.m
//  Sum Fibonacci Machine
//
//  Created by mz on 3/8/15.
//  Copyright (c) 2015 mz. All rights reserved.
//

#import "AppDelegate.h"
#import "Fib.h"

@implementation AppDelegate

@synthesize textField;
@synthesize displayField;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    Fib *aFib = [[Fib alloc] init];
    [self setFib:aFib];
}

- (IBAction)fibLimit:(id)sender {
    int unsigned newValue = [sender intValue];
    int unsigned overallSum = 0;
    int unsigned summedValue = 0;
    int unsigned lastValue = 0;
    int unsigned currentValue = 1;
    int unsigned i = 0;
    NSMutableString *aString = [NSMutableString stringWithFormat:@"" ];
    while (i < newValue) {
        summedValue = lastValue + currentValue;
        lastValue = currentValue;
        overallSum += currentValue = summedValue;
        [aString appendFormat:@"%d ", summedValue];
        i++;
    }
    [self.fib setLimit:overallSum];
    [self.sequenceField setStringValue:aString];
    [self updateView];
}

- (void) updateView{
    int limit = [self.fib limit];
    [self.displayField setIntegerValue:limit];
    
}
@end

//
//  main.m
//  Sum Fibonacci Machine
//
//  Created by mz on 3/8/15.
//  Copyright (c) 2015 mz. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}

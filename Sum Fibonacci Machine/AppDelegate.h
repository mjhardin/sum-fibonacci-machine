//
//  AppDelegate.h
//  Sum Fibonacci Machine
//
//  Created by mz on 3/8/15.
//  Copyright (c) 2015 mz. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class Fib;
@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSTextField *textField;
@property (weak) IBOutlet NSTextField *displayField;
@property (weak) IBOutlet NSTextField *sequenceField;

@property (strong) Fib *fib;

- (IBAction)fibLimit:(id)sender;

- (void)updateView;


@end

//
//  Fib.h
//  Sum Fibonacci Machine
//
//  Created by mz on 3/8/15.
//  Copyright (c) 2015 mz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fib : NSObject
@property (assign) int unsigned limit;

@end
